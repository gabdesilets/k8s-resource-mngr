package main

import (
	"bytes"
	"context"
	"flag"
	"fmt"
	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"
	"html/template"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"time"
)

var localNamespaces []v1.Namespace

func main() {
	go initK8sCollector()
	initApi()
}

// initApi setup the rest api routes and start said api on :8001
func initApi() {
	// channel to listen for events like ctrl-c or other possible exiting events
	done := make(chan bool)
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)

	e := echo.New()

	// define router endpoints
	e.GET("/", list)
	e.GET("/health", health)

	go func() {
		<-quit
		log.Info("Server is shutting down...")

		ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()

		if err := e.Shutdown(ctx); err != nil {
			log.Fatalf("Could not gracefully shutdown the server: %v\n", err)
		}
		close(done)
	}()

	log.Panic(e.Start(":3000"))
}

func initK8sCollector() {
	var config *rest.Config
	if os.Getenv("CLUSTER") == "outside" {
		config = outsideClusterConfig()
	} else {
		config = insideClusterConfig()
	}
	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	for {
		// get namespaces cluster
		namespaces, err := clientset.CoreV1().Namespaces().List(context.TODO(), metav1.ListOptions{
			LabelSelector: "environment=pull-request",//TODO parameterizable
		})
		if errors.IsNotFound(err) {
			fmt.Printf("No namespaces found for cluster\n")
		} else if statusError, isStatus := err.(*errors.StatusError); isStatus {
			fmt.Printf("Error getting namespace %v\n", statusError.ErrStatus.Message)
		} else if err != nil {
			panic(err.Error())
		}

		localNamespaces = []v1.Namespace{}
		for i := 0; i < len(namespaces.Items); i++ {
			localNamespaces = append(localNamespaces, namespaces.Items[i])
		}
		fmt.Printf("There are %d pr-env in the cluster\n", len(localNamespaces))
		time.Sleep(1 * time.Minute)
	}
}

func insideClusterConfig() *rest.Config {
	//// creates the in-cluster config
	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err.Error())
	}
	return config
}

func outsideClusterConfig() *rest.Config {
	var kubeconfig *string
	if home := homedir.HomeDir(); home != "" {
		kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	} else {
		kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	}
	flag.Parse()

	// use the current context in kubeconfig
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		panic(err.Error())
	}
	return config
}

type PRLink struct {
	Link string
	Jira string
}

func list(c echo.Context) error {
	var links []PRLink
	for _, namespace := range localNamespaces {
		links = append(links, struct {
			Link string
			Jira string
		}{Link: fmt.Sprintf("https://%s-%s.test", namespace.Name, namespace.Labels["jira"]), Jira: namespace.Labels["jira"]})
	}
	t, err := template.New("links").Parse("{{ range $link := .Links }} <li> &#128640; <a href={{$link.Link}} target='_blank'> Environnement de: {{ $link.Jira }}</a> &#128640;</li> {{ end }}")
	if err != nil {
		return err
	}
	buf := new(bytes.Buffer)
	err = t.ExecuteTemplate(buf, "links", struct {
		Links []PRLink `json:"links"`
	}{Links: links})
	if err != nil {
		return err
	}
	return c.HTML(http.StatusOK, buf.String())
}

func health(c echo.Context) error {
	return c.JSON(http.StatusOK, "healthy")
}

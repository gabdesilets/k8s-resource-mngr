# Namespace lister

Show me your namespace!

## Code

Got lazy, so if you want to run from outside a k8s cluster -> `CLUTER=outside go run .`.

It'll use your `~/.kubeconfig` to connect to the cluster

`CLUTER=outside` va dire a l'app d'initialiser avec la config kubernetes local dans le root par defaut. comme on roule pas
a l'interieur du k8s cluster.

You might need to create cluster role and cluster role bind to your resource to `[get, list]` namespace

# TODO:

[] -label filter from params vs hardcoded one
[] - add k8s resource to list
FROM golang:1.17-buster AS golang
WORKDIR /src
COPY go.mod go.sum ./
RUN go mod download
COPY . ./
RUN go build -o pr2d2

FROM debian:buster-slim
WORKDIR /app
CMD ./pr2d2
COPY --from=golang /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=golang /src/pr2d2 /app/pr2d2
